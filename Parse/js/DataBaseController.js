$(document).ready( () => {
	SearchDatas_1();
	SearchDatas_2();
	SearchDatas_3();
	CheckBoxultM('#DSrcMainCheck','#DSrcChecks');
	CheckBoxultM('#DPersonalMainCheck','#DPersonalChecks');
	CheckBoxultM('#DHistoryMainCheck','#DHistoryChecks');
	CheckBoxultM('#DDiagnosisMainCheck','#DDiagnosisChecks');
	CheckBoxultM('#DSyndromeMainCheck','#DSyndromeChecks');
	$.ajax({
		type: 'POST',
		url: 'PatientCaseDataBase.do',
		data: {
			"sign": "1",
			"SearchDetail1": ""
		},
		success: response => {
			var result = $.parseJSON(response);
			let mount = result.length;
			let TagContent = "<h3>預覽 ： " +
					"<button class=\"btn btn-danger\" style=\"float:right\" onclick=\"DataBase_Syndromes_Analysis_Refresh(confirm(\'重新辨證所有病例，大約需要等待"+Math.ceil(mount/10)+"分鐘。請問是否確定要重新辨證所有病例?\') );\">" +
					"<span class=\"glyphicon glyphicon-refresh\"></span> 病例重新辨證</button>" +
					"<button class=\"btn btn-warning\" style=\"float:right\" onclick=\"FullSymptomsStandard(confirm(\'標準化所有存在於資料庫之症狀，大約需要等待"+Math.ceil(mount/100)+"分鐘。請問是否確定要標準化所有存在於資料庫之症狀?\') );\">" +
					"<span class=\"glyphicon glyphicon-refresh\"></span> 症狀重新標準化 </button></h3>"
			$('#PreviewTag').html(TagContent);
		},
		error: err => {
			console.log("SearchData_Ready_err:", err);
		}
	});
});

//監測查詢頁面之查詢輸入框
$("#CSearchDetail").on('input', () => {SearchDatas_1();} );

//監測維護頁面之查詢輸入框
$("#ISearchDetail").on('input', () => {SearchDatas_3();} );

//限制輸入數字
function ValidateNumber(e, pnumber)
{
    if (!/^\d+$/.test(pnumber))
    {
        var newValue =/^\d+/.exec(e.value);         
        if (newValue != null)         
        {             
            e.value = newValue;        
        }      
        else     
        {          
            e.value = "";    
        } 
    }
    return false;
}

//限制輸入含小數點之數字
function ValidateFloat(e, pnumber)
{
    if (!/^\d+[.]?\d*$/.test(pnumber))
    {
        var newValue = /^\d+[.]?\d*/.exec(e.value);         
        if (newValue != null)         
        {             
            e.value = newValue;        
        }      
        else     
        {          
            e.value = "";    
        } 
    }
    return false;
}

//CheckBox 外觀
function CheckBoxMtul(CheckBoxID, ulID) {

	if ($(CheckBoxID).prop("checked")) {
		$(ulID + " input[type='checkbox']").prop("checked", true);
	} else {
		$(ulID + " input[type='checkbox']").prop("checked", false);
	}

}

function CheckBoxultM(CheckBoxID, ulID) {

	let count = 0;
	let ulIDlen = $(ulID + " input[type='checkbox']").length

	for (let i = 0; i < ulIDlen; i++) {
		if ($(ulID + " input[type='checkbox']")[i].checked) {
			count++;
		}
	}

	if (count == ulIDlen) {
		$(CheckBoxID).prop("indeterminate", false)
		$(CheckBoxID).prop("checked", true);
	} else if (count == 0) {
		$(CheckBoxID).prop("indeterminate", false)
		$(CheckBoxID).prop("checked", false);
	} else {
		$(CheckBoxID).prop("checked", false);
		$(CheckBoxID).prop("indeterminate", true);
	}


}

//時間string="西元年月日時分秒"
function NowTimeString(){
	
	let now = new Date();
	let month = now.getMonth()+1;
	let date = now.getDate();
	let hours = now.getHours();
	let minutes = now.getMinutes();
	let seconds = now.getSeconds();
	let timestring = now.getFullYear()+(month<10?("0"+month):month)+(date<10?("0"+date):date)+"";
	timestring += (hours<10?("0"+hours):hours)+(minutes<10?("0"+minutes):minutes)+(seconds<10?("0"+seconds):seconds)+"";
	return timestring;
}

function QuickCheckInputAlert(CheckID){
	!$('#'+CheckID).val()?$('#'+CheckID).addClass("Inputalert"):$('#'+CheckID).removeClass("Inputalert");
}

function QuickCheckAges(){
	QuickCheckInputAlert('IAges');
	if( $('#IAges').val() != "" ){
		let now = new Date();
		if($('#IAges').val() > now.getFullYear() || $('#IAges').val() < 1000 || $('#IAges').val().length != 4 ){
			$('#IAges').addClass("Inputalert");
		}else{
			$('#IAges').removeClass("Inputalert");
		}
	}
}

function QuickCheckGender(){
	if ( $('#IGender').val() != "男" && $('#IGender').val() !=　"女" && $('#IGender').val() !=　"" ){
		$('#IGender').addClass("Inputalert");
	}else{
		$('#IGender').removeClass("Inputalert");
	}
}

function QuickCheckAge(){
	if( $('#IAge').val() != "" ){
		if ( $('#IAge').val() < 0 || $('#IAge').val() > 150 ){
			$('#IAge').addClass("Inputalert");
		}else{
			$('#IAge').removeClass("Inputalert");
		}
	}
}

function QuickCheckHeight(){
	if( $('#IHeight').val() != "" ){
		if ( $('#IHeight').val() < 20 || $('#IHeight').val() > 250 ){
			$('#IHeight').addClass("Inputalert");
		}else{
			$('#IHeight').removeClass("Inputalert");
		}
	}
}

function QuickCheckWeight(){
	if ( $('#IWeight').val() != ""){
		if ( $('#IWeight').val() < 0 || $('#IWeight').val() > 250 ){
			$('#IWeight').addClass("Inputalert");
		}else{
			$('#IWeight').removeClass("Inputalert");
		}
	}
}

//維護檢查用 含外觀提醒
function InsertCheck(){
	let AlertContent = "" ;
	
	!$('#ITitle').val()?$('#ITitle').addClass("Inputalert"):$('#ITitle').removeClass("Inputalert");
	!$('#IAuthor').val()?$('#IAuthor').addClass("Inputalert"):$('#IAuthor').removeClass("Inputalert");
	!$('#IAges').val()?$('#IAges').addClass("Inputalert"):$('#IAges').removeClass("Inputalert");
	!$('#IPublish').val()?$('#IPublish').addClass("Inputalert"):$('#IPublish').removeClass("Inputalert");
	
	if ( !$('#ITitle').val() || !$('#IAuthor').val() || !$('#IAges').val() || !$('#IPublish').val() ) {
		AlertContent += "◆篇名、◆作者、◆年代、◆出處 為必填欄位！\n" ;
	}
	
	if( $('#IAges').val() != "" ){
		let now = new Date();
		if($('#IAges').val() > now.getFullYear() || $('#IAges').val() < 1000 || $('#IAges').val().length != 4 ){
			AlertContent += "請輸入正確出版年代！\n" ;
			$('#IAges').addClass("Inputalert");
		}else{
			$('#IAges').removeClass("Inputalert");
		}
	}
		
	if ( $('#IGender').val() != "男" && $('#IGender').val() !=　"女" && $('#IGender').val() !=　"" ){
		$('#IGender').addClass("Inputalert");
		AlertContent += "請輸入正確性別！ 不明請保持空白！\n" ;
	}else{
		$('#IGender').removeClass("Inputalert");
	}
	
	if( $('#IAge').val() != "" ){
		if ( $('#IAge').val() < 0 || $('#IAge').val() > 150 ){
			$('#IAge').addClass("Inputalert");
			AlertContent += "請輸入正確年齡！不明請保持空白！\n" ;
		}else{
			$('#IAge').removeClass("Inputalert");
		}
	}
	
	if( $('#IHeight').val() != "" ){
		if ( $('#IHeight').val() < 20 || $('#IHeight').val() > 250 ){
			$('#IHeight').addClass("Inputalert");
			AlertContent += "請輸入正確身高！ 不明請保持空白！\n" ;
		}else{
			$('#IHeight').removeClass("Inputalert");
		}
	}

	if ( $('#IWeight').val() != ""){
		if ( $('#IWeight').val() < 0 || $('#IWeight').val() > 250 ){
			$('#IWeight').addClass("Inputalert");
			AlertContent += "請輸入正確體重！ 不明請保持空白！\n" ;
		}else{
			$('#IWeight').removeClass("Inputalert");
		}
	}

	return AlertContent;
}

//查詢分頁 搜尋欄 to 篩選結果
function SearchDatas_1() {

	//單頁顯示筆數
	let PageUnit = 10;
	
	$.ajax({
		type: 'POST',
		url: 'PatientCaseDataBase.do',
		data: {
			"sign": "1",
			"SelectKey1": $('#CSelectKey').val(),
			"SearchDetail1": $('#CSearchDetail').val()
		},
		success: response => {
			var result = $.parseJSON(response);
			let PageCounter = 1;
			let table = "";
			let pagination = "";
			if (result.length != 0) {
				for (var num = 0; num < result.length; num++) {
					if ( num % PageUnit == 0){
						if (PageCounter == 1){
							table += "<div id='Cpage"+PageCounter+"' class='tab-pane fade in active'><table><tr><th>篇名</th><th>出處</th></tr>";
							pagination += "<li class='active'><a data-toggle='tab' href='#Cpage"+PageCounter+"'>"+PageCounter+"</a></li>";
							PageCounter++;
						}else{
							table += "</table></div><div id='Cpage"+PageCounter+"' class='tab-pane fade'><table><tr><th>篇名</th><th>出處</th></tr>";
							pagination += "<li><a data-toggle='tab' href='#Cpage"+PageCounter+"'>"+PageCounter+"</a></li>";
							PageCounter++;
						}
					}
					table += "<tr onclick='SearchDataRead_1(" + result[num].Id + ")'><td>"
					
					//超過26字部分則以...取代
					if (result[num].Title.length > 26 ){
						table += result[num].Title.slice(0,26) + "..." ;
					}else{
						table += result[num].Title ;
					}
					table += "</td><td>" + result[num].Publish + "</td></tr>";
				}
				
				//補齊每頁格子
				if( result.length % PageUnit != 0 ){
					for(let i = 0 ; i < PageUnit - result.length % PageUnit ; i++ ){
						table += "<tr style='height:37px;'><td></td><td></td></tr>";
					}
				}
				
				
				table += "</table></div>";
				
				$('#pagination_1').show();
				$('#pagination_1').html(pagination);
				
			} else {
				table = "<h1>未檢索到任何到相關結果<h1>";
				$('#pagination_1').hide();
			}
			$('#CCaseSearchTable').html(table);
		},
		error: err => {
			console.log("SearchData_1_err:", err);
		}
	});
}

//刷新檢索病例頁面之症狀標準化結果
function PickedSymptomsStandard(CaseID){
	let temp = $('#StandardSymptomTag').html();
	$('#StandardSymptomTag').html("<h3>已標準化症狀 ： <small><span class='glyphicon glyphicon-refresh w3-spin'></span></small></h3>");
	
	$.ajax({
		type: 'POST',
		url: 'PatientCaseDataBase.do',
		data: {
			"sign": "4",
			"Id": CaseID
		},
		success: response => {
			var result = $.parseJSON(response);
			var Mixed_Cut_Diagnosis = result[0].Cut_Inspection + result[0].Cut_Listen_and_Smelling_Examination + result[0].Cut_Inquiry + result[0].Cut_Palpation;
			
			$.ajax({
				url: "SymptomsStandardbyMySQL",
				type: "POST",
				data: {
					"Original_Symptoms": Mixed_Cut_Diagnosis　
				},
				dataType: "json",
				success: response => {
					var text_indb = "";
					var text_notindb = "";
					response.map( item => {
						for (i = 0; i < item.modified_Data_list.length; i++) {
							if (item.modified_Data_list[i].standard_symptoms != "不存在資料庫") {
								text_indb += item.modified_Data_list[i].standard_symptoms + "。";
							} else {
								if (item.original_symptoms == "") continue;
								text_notindb += item.original_symptoms + "。";
							}
						}
					});
					
					$.ajax({
						type: 'POST',
						url: 'PatientCaseDataBase.do',
						data: {
							"sign": "10",
							"Id": CaseID ,
							"Symptoms_Standard": text_indb ,
							"Symptoms_Unstandard": text_notindb
						},success: response => {
							$("#CDiagnosisArdStandard").html(text_indb);
							$("#CDiagnosisArdStandard2").html(text_indb);
							$("#CDiagnosisUnStandard").html(text_notindb);
							$('#StandardSymptomTag').html(temp);
						},error: err => {
							console.log("findstandard_update_database_err:", err);
							return;
						}
					});

				},
				error: err => {
					console.log("findstandard_ajax_err:", err);
				}
			});
		},
		error: err => {
			console.log("PickedSymptomsStandard_err:", err);
		}
	});
}

//刷新資料庫中全病例的症狀標準化結果
function FullSymptomsStandard(ConfirmTag){
	if(ConfirmTag){
		$.ajax({
			type: 'POST',
			url: 'PatientCaseDataBase.do',
			data: {
				"sign": "2"
			},
			success: response => {
				var result = $.parseJSON(response);
				for (let i = 0; i < result.length ; i++){
					SingleCaseStandard(result[i].Id);
				}
			},error: err => {
				console.log("FullSymptomsStandard_err:", err);
			}
		});
	}
}

//查詢分頁 篩選結果 to 資料呈現
function SearchDataRead_1(Id) {
	$.ajax({
		type: 'POST',
		url: 'PatientCaseDataBase.do',
		data: {
			"sign": "3",
			"Id": Id
		},
		success: response => {
			var result = $.parseJSON(response);

			if (result[0].Gender == "") result[0].Gender = "-";
			if (result[0].Age == "") result[0].Age = "-";
			if (result[0].Height == "") result[0].Height = "-";
			if (result[0].Weight == "") result[0].Weight = "-";
			if (result[0].Marriage == "") result[0].Marriage = "-";
			if (result[0].Place_of_Residence == "") result[0].Place_of_Residence = "-";
			if (result[0].Career == "") result[0].Career = "-";

			$('#CTitle').html(result[0].Title);
			$('#CAuthor').html(result[0].Author);
			$('#CAges').html(result[0].Ages);
			$('#CPublish').html(result[0].Publish);

			$('#CGender').html(result[0].Gender);
			$('#CAge').html(result[0].Age);
			$('#CHeight').html(result[0].Height);
			$('#CWeight').html(result[0].Weight);
			$('#CMarriage').html(result[0].Marriage);
			$('#CPOResidence').html(result[0].Place_of_Residence);
			$('#CCareer').html(result[0].Career);

			$('#CChiefComplaint').html(result[0].Chief_Complaint);
			
			//Modal Content
			$('#CCurrentMedicalHistoryContent').html(result[0].Current_Medical_History);
			$('#CPastMedicalHistoryContent').html(result[0].Past_Medical_History);
			$('#CPersonalHistoryContent').html(result[0].Personal_History);
			$('#CFamilyHistoryContent').html(result[0].Family_History);
			
			//Button title
			//document.getElementById("CCMHBtn").title = result[0].Current_Medical_History;
			//document.getElementById("CPMHBtn").title = result[0].Past_Medical_History;
			//document.getElementById("CPHBtn").title = result[0].Personal_History;
			//document.getElementById("CFHBtn").title = result[0].Family_History;

			$('#CObservationDiagnosisOrigin').html(result[0].Inspection_Origin);
			$('#CListenDiagnosisOrigin').html(result[0].Listen_and_Smelling_Examination_Origin);
			$('#CAskDiagnosisOrigin').html(result[0].Inquiry_Origin);
			$('#CPulseDiagnosisOrigin').html(result[0].Palpation_Origin);

			$('#CDiseaseDifferentiation').html(result[0].Disease_Differentiation);
			$('#CSyndromeDifferentiation').html(result[0].Syndrome_Differentiation);
			$('#CTreatmentPrinciples').html(result[0].Treatment_Principles);
			$('#CPrescription').html(result[0].Prescription);
			$('#CWesternMedicalDiagnosis').html(result[0].Western_Medical_Diagnosis);
			
			$.ajax({
				type: 'POST',
				url: 'PatientCaseDataBase.do',
				data: {
					"sign": "4",
					"Id": Id
				},
				success: response => {
					var result = $.parseJSON(response);
					$('#CObservationDiagnosis').html(result[0].Cut_Inspection);
					$('#CListenDiagnosis').html(result[0].Cut_Listen_and_Smelling_Examination);
					$('#CAskDiagnosis').html(result[0].Cut_Inquiry);
					$('#CPulseDiagnosis').html(result[0].Cut_Palpation);

					$('#CDiagnosisArdStandard').html(result[0].Symptoms_Standard);
					$('#CDiagnosisArdStandard2').html(result[0].Symptoms_Standard);
					$('#CDiagnosisUnStandard').html(result[0].Symptoms_Unstandard);
					$('#DifferentationResult').html(result[0].Syndromes_Analysis_Result);
					
					$('#StandardSymptomTag').html("<h3>已標準化症狀 ： <small><span class='glyphicon glyphicon-refresh' onclick='PickedSymptomsStandard("+Id+")'></span></small> </h3>");
					$('#SyndromesAnalysisTag').html("<h3>辨證使用之標準症狀 ： <small><span class='glyphicon glyphicon-refresh' onclick='Single_Syndromes_Analysis("+Id+")'></span></small> </h3>");
					
				},error: err => {
					console.log("SearchDataRead_Standard_1_err:", err);
				}
			});
		},
		error: err => {
			console.log("SearchDataRead_1_err:", err);
		}
	});
}

//Checkbox 勾選部分 轉換成 Array 
function GetArrayfromCheckbox(){
	let Array = [];
	$("#CheckBoxCorner input[type='checkbox']").each( function(){
		if($(this).prop("checked")){
			//Checkbox value 預設 = "on"
			if($(this).val() != "on"){
				Array.push($(this).val());
			}
		}
	});
	return Array;
}

//製作 MySql 語法 Select 部分
function GetSelectStringfromCheckbox(){
	
	let SelectString = "";
	let Array = GetArrayfromCheckbox();
	if ( Array.length == 0 ) return "Title,Author,Ages,Publish,patient_case_database.Gender,Age,Height,Weight,Marriage,Place_of_Residence,Career,Chief_Complaint,Current_Medical_History,Past_Medical_History,Personal_History,Family_History,Inspection,Listen_and_Smelling_Examination,Inquiry,Palpation,Western_Medical_Diagnosis,Disease_Differentiation,Syndrome_Differentiation,Treatment_Principles,Prescription";
	SelectString = Array[0];
	for(let i=1 ; i < Array.length ; i++ ){
		SelectString += ','+Array[i];
	}
	return SelectString;
}

function SearchDatas_2(){
	
	let SelectArray = GetArrayfromCheckbox();
	let SelectString = GetSelectStringfromCheckbox();
	
	$.ajax({
		type: 'POST',
		url: 'PatientCaseDataBase.do',
		data: {
			"sign": "5",
			"MainKey": SelectString,
			"SelectKey1": $('#DSelectKey1').val(),
			"SearchDetail1": $('#DSearchDetail1').val(),
			"SwitchBtn1": $('#DSwitchBtn1').prop('checked'),
			"SelectKey2": $('#DSelectKey2').val(),
			"SearchDetail2": $('#DSearchDetail2').val(),
			"SwitchBtn2": $('#DSwitchBtn2').prop('checked'),
			"SelectKey3": $('#DSelectKey3').val(),
			"SearchDetail3": $('#DSearchDetail3').val()
		},
		success: response => {
			
			var result = $.parseJSON(response);
			
			let SwitchArray = [
				SelectString.search("Title")<0?false:true,
				SelectString.search("Author")<0?false:true,
				SelectString.search("Ages")<0?false:true,
				SelectString.search("Publish")<0?false:true,
				SelectString.search("Gender")<0?false:true,
				SelectString.slice(SelectString.search("Ages")+1).search("Age")<0?false:true,
				SelectString.search("Height")<0?false:true,
				SelectString.search("Weight")<0?false:true,
				SelectString.search("Marriage")<0?false:true,
				SelectString.search("Place_of_Residence")<0?false:true,
				SelectString.search("Career")<0?false:true,
				SelectString.search("Chief_Complaint")<0?false:true,
				SelectString.search("Current_Medical_History")<0?false:true,
				SelectString.search("Past_Medical_History")<0?false:true,
				SelectString.search("Personal_History")<0?false:true,
				SelectString.search("Family_History")<0?false:true,
				SelectString.search("Ori_Inspection")<0?false:true,
				SelectString.search("Ori_Listen_and_Smelling_Examination")<0?false:true,
				SelectString.search("Ori_Inquiry")<0?false:true,
				SelectString.search("Ori_Palpation")<0?false:true,
				SelectString.search("Cut_Inspection")<0?false:true,
				SelectString.search("Cut_Listen_and_Smelling_Examination")<0?false:true,
				SelectString.search("Cut_Inquiry")<0?false:true,
				SelectString.search("Cut_Palpation")<0?false:true,
				SelectString.search("Symptoms_Standard")<0?false:true,
				SelectString.search("Symptoms_Unstandard")<0?false:true,
				SelectString.search("Western_Medical_Diagnosis")<0?false:true,
				SelectString.search("Disease_Differentiation")<0?false:true,
				SelectString.search("Syndrome_Differentiation")<0?false:true,
				SelectString.search("Syndromes_Analysis_Result")<0?false:true,
				SelectString.search("Treatment_Principles")<0?false:true,
				SelectString.search("Prescription")<0?false:true
				];
			
			//預覽部分
			let TableHead = SelectString.replace(/,/g,"</th><th noWrap>")
										.replace("Title","標題")
										.replace("Author","作者")
										.replace("Ages","年代")
										.replace("Publish","出處")
										.replace("Gender","性別")
										.replace("Age","年齡")
										.replace("Height","身高")
										.replace("Weight","體重")
										.replace("Marriage","婚姻狀態")
										.replace("Place_of_Residence","居住地")
										.replace("Career","職業")
										.replace("Chief_Complaint","主訴")
										.replace("Current_Medical_History","現病史")
										.replace("Past_Medical_History","過去病史")
										.replace("Personal_History","個人史")
										.replace("Family_History","家族病史")
										.replace("Ori_Inspection","原文望診")
										.replace("Ori_Listen_and_Smelling_Examination","原文聞診")
										.replace("Ori_Inquiry","原文問診")
										.replace("Ori_Palpation","原文切診")
										.replace("Cut_Inspection","原始症狀望診")
										.replace("Cut_Listen_and_Smelling_Examination","原始症狀聞診")
										.replace("Cut_Inquiry","原始症狀問診")
										.replace("Cut_Palpation","原始症狀切診")
										.replace("Symptoms_Standard","已標準化症狀")
										.replace("Symptoms_Unstandard","未標準化症狀")
										.replace("Western_Medical_Diagnosis","西醫診斷")
										.replace("Disease_Differentiation","病名")
										.replace("Syndrome_Differentiation","證候")
										.replace("Syndromes_Analysis_Result","系統辨證結果")
										.replace("Treatment_Principles","治則")
										.replace("Prescription","處方");
			
			let table = "<thead><tr><th noWrap>" + TableHead + "</th></tr></thead><tbody>";
			
			let MaxPreviewNumber = 24 ; //預覽顯示筆數
			let MaxWordLength = 24 ; //每格字數上限 超過的部分以"..."表示省略

			for (let num = 0; num < Math.min(result.length, MaxPreviewNumber) ; num++) {
				table += "<tr>";
				if (SwitchArray[0]) table += "<td noWrap>" + (result[num].Title.length<MaxWordLength?result[num].Title:result[num].Title.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[1]) table += "<td noWrap>" + result[num].Author + "</td>";
				if (SwitchArray[2]) table += "<td noWrap>" + result[num].Ages + "</td>";
				if (SwitchArray[3]) table += "<td noWrap>" + result[num].Publish + "</td>";
				if (SwitchArray[4]) table += "<td noWrap>" + result[num].Gender + "</td>";
				if (SwitchArray[5]) table += "<td noWrap>" + result[num].Age + "</td>";
				if (SwitchArray[6]) table += "<td noWrap>" + result[num].Height + "</td>";
				if (SwitchArray[7]) table += "<td noWrap>" + result[num].Weight + "</td>";
				if (SwitchArray[8]) table += "<td noWrap>" + result[num].Marriage + "</td>";
				if (SwitchArray[9]) table += "<td noWrap>" + result[num].Place_of_Residence + "</td>";
				if (SwitchArray[10]) table += "<td noWrap>" + result[num].Career + "</td>";
				if (SwitchArray[11]) table += "<td noWrap>" + (result[num].Chief_Complaint.length<MaxWordLength?result[num].Chief_Complaint:result[num].Chief_Complaint.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[12]) table += "<td noWrap>" + (result[num].Current_Medical_History.length<MaxWordLength?result[num].Current_Medical_History:result[num].Current_Medical_History.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[13]) table += "<td noWrap>" + (result[num].Past_Medical_History.length<MaxWordLength?result[num].Past_Medical_History:result[num].Past_Medical_History.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[14]) table += "<td noWrap>" + (result[num].Personal_History.length<MaxWordLength?result[num].Personal_History:result[num].Personal_History.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[15]) table += "<td noWrap>" + (result[num].Family_History.length<MaxWordLength?result[num].Family_History:result[num].Family_History.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[16]) table += "<td noWrap>" + (result[num].Ori_Inspection.length<MaxWordLength?result[num].Ori_Inspection:result[num].Ori_Inspection.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[17]) table += "<td noWrap>" + (result[num].Ori_Listen_and_Smelling_Examination.length<MaxWordLength?result[num].Ori_Listen_and_Smelling_Examination:result[num].Ori_Listen_and_Smelling_Examination.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[18]) table += "<td noWrap>" + (result[num].Ori_Inquiry.length<MaxWordLength?result[num].Ori_Inquiry:result[num].Ori_Inquiry.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[19]) table += "<td noWrap>" + (result[num].Ori_Palpation.length<MaxWordLength?result[num].Ori_Palpation:result[num].Ori_Palpation.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[20]) table += "<td noWrap>" + (result[num].Cut_Inspection.length<MaxWordLength?result[num].Cut_Inspection:result[num].Cut_Inspection.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[21]) table += "<td noWrap>" + (result[num].Cut_Listen_and_Smelling_Examination.length<MaxWordLength?result[num].Cut_Listen_and_Smelling_Examination:result[num].Cut_Listen_and_Smelling_Examination.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[22]) table += "<td noWrap>" + (result[num].Cut_Inquiry.length<MaxWordLength?result[num].Cut_Inquiry:result[num].Cut_Inquiry.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[23]) table += "<td noWrap>" + (result[num].Cut_Palpation.length<MaxWordLength?result[num].Cut_Palpation:result[num].Cut_Palpation.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[24]) table += "<td noWrap>" + (result[num].Symptoms_Standard.length<MaxWordLength?result[num].Symptoms_Standard:result[num].Symptoms_Standard.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[25]) table += "<td noWrap>" + (result[num].Symptoms_Unstandard.length<MaxWordLength?result[num].Symptoms_Unstandard:result[num].Symptoms_Unstandard.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[26]) table += "<td noWrap>" + (result[num].Western_Medical_Diagnosis.length<MaxWordLength?result[num].Western_Medical_Diagnosis:result[num].Western_Medical_Diagnosis.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[27]) table += "<td noWrap>" + result[num].Disease_Differentiation + "</td>";
				if (SwitchArray[28]) table += "<td noWrap>" + result[num].Syndrome_Differentiation + "</td>";
				if (SwitchArray[29]) table += "<td noWrap>" + (result[num].Syndromes_Analysis_Result.length<MaxWordLength?result[num].Syndromes_Analysis_Result:result[num].Syndromes_Analysis_Result.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[30]) table += "<td noWrap>" + (result[num].Treatment_Principles.length<MaxWordLength?result[num].Treatment_Principles:result[num].Treatment_Principles.slice(0,MaxWordLength)+"...") + "</td>";
				if (SwitchArray[31]) table += "<td noWrap>" + (result[num].Prescription.length<MaxWordLength?result[num].Prescription:result[num].Prescription.slice(0,MaxWordLength)+"...") + "</td>";
				table += "</tr>";
			}
			
			table += "</tbody>"
			$('#DCaseSearchTable').html(table);
			
			
			//CSV 部分用
			let CSVHead = SelectString.replace("Title","\"標題\"")
										.replace("Author","\"作者\"")
										.replace("Ages","\"年代\"")
										.replace("Publish","\"出處\"")
										.replace("patient_case_database.Gender","\"性別\"")
										.replace("Age","\"年齡\"")
										.replace("Height","\"身高\"")
										.replace("Weight","\"體重\"")
										.replace("Marriage","\"婚姻狀態\"")
										.replace("Place_of_Residence","\"居住地\"")
										.replace("Career","\"職業\"")
										.replace("Chief_Complaint","\"主訴\"")
										.replace("Current_Medical_History","\"現病史\"")
										.replace("Past_Medical_History","\"過去病史\"")
										.replace("Personal_History","\"個人史\"")
										.replace("Family_History","\"家族病史\"")
										.replace("Ori_Inspection","\"原文望診\"")
										.replace("Ori_Listen_and_Smelling_Examination","\"原文聞診\"")
										.replace("Ori_Inquiry","\"原文問診\"")
										.replace("Ori_Palpation","\"原文切診\"")
										.replace("Cut_Inspection","\"原始症狀望診\"")
										.replace("Cut_Listen_and_Smelling_Examination","\"原始症狀聞診\"")
										.replace("Cut_Inquiry","\"原始症狀問診\"")
										.replace("Cut_Palpation","\"原始症狀切診\"")
										.replace("Symptoms_Standard","\"已標準化症狀\"")
										.replace("Symptoms_Unstandard","\"未標準化症狀\"")
										.replace("Western_Medical_Diagnosis","\"西醫診斷\"")
										.replace("Disease_Differentiation","\"病名\"")
										.replace("Syndrome_Differentiation","\"證候\"")
										.replace("Syndromes_Analysis_Result","\"系統辨證結果\"")
										.replace("Treatment_Principles","\"治則\"")
										.replace("Prescription","\"處方\"");
			
			let CSVBody = "";
			for (let num = 0; num < result.length ; num++) {
				let SwitchUp = 0 ;
				if (SwitchArray[0]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Title.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[1]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Author.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[2]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Ages.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[3]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Publish.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[4]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Gender.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[5]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Age.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[6]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Height.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[7]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Weight.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[8]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Marriage.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[9]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Place_of_Residence.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[10]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Career.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[11]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Chief_Complaint.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[12]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Current_Medical_History.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[13]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Past_Medical_History.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[14]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Personal_History.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[15]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Family_History.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[16]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Ori_Inspection.replace(/\"/g,"\"\""); SwitchUp++; } 
				if (SwitchArray[17]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Ori_Listen_and_Smelling_Examination.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[18]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Ori_Inquiry.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[19]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Ori_Palpation.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[20]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Cut_Inspection.replace(/\"/g,"\"\""); SwitchUp++; } 
				if (SwitchArray[21]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Cut_Listen_and_Smelling_Examination.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[22]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Cut_Inquiry.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[23]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Cut_Palpation.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[24]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Symptoms_Standard.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[25]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Symptoms_Unstandard.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[26]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Western_Medical_Diagnosis.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[27]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Disease_Differentiation.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[28]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Syndrome_Differentiation.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[29]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Syndromes_Analysis_Result.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[30]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Treatment_Principles.replace(/\"/g,"\"\""); SwitchUp++; }
				if (SwitchArray[31]){ CSVBody += (SwitchUp!=0?"\",\"":"\"") + result[num].Prescription.replace(/\"/g,"\"\""); SwitchUp++; }
				CSVBody += "\"\n";
			}

			$('#CSV_temp').html(CSVHead + "\n" + CSVBody);
			
		},
		error: err => {
			console.log("SearchData_2_err:", err);
		}
	});
}


//維護分頁 搜尋欄 to 篩選結果
function SearchDatas_3() {
	
	//單頁顯示筆數
	let PageUnit = 15 ;
	
	$.ajax({
		type: 'POST',
		url: 'PatientCaseDataBase.do',
		data: {
			"sign": "1",
			"SelectKey1": $('#ISelectKey').val(),
			"SearchDetail1": $('#ISearchDetail').val()
		},
		success: response => {
			var result = $.parseJSON(response);
			let PageCounter = 1;
			let table = "";
			let pagination = "";
			if (result.length != 0) {
				for (var num = 0; num < result.length; num++) {
					if ( num % PageUnit == 0){
						if (PageCounter == 1){
							table += "<div id='Ipage"+PageCounter+"' class='tab-pane fade in active'><table><tr><th>篇名</th><th>出處</th></tr>";
							pagination += "<li class='active'><a data-toggle='tab' href='#Ipage"+PageCounter+"'>"+PageCounter+"</a></li>";
							PageCounter++;
						}else{
							table += "</table></div><div id='Ipage"+PageCounter+"' class='tab-pane fade'><table><tr><th>篇名</th><th>出處</th></tr>";
							pagination += "<li><a data-toggle='tab' href='#Ipage"+PageCounter+"'>"+PageCounter+"</a></li>";
							PageCounter++;
						}
					}
					table += "<tr onclick='SearchDataRead_3(" + result[num].Id + ")' data-dismiss='modal'><td>";
					
					//超過26字部分則以...取代
					if (result[num].Title.length > 26 ){
						table += result[num].Title.slice(0,25) + "..." ;
					}else{
						table += result[num].Title ;
					}
					table += "</td><td>" + result[num].Publish + "</td></tr>";
				}
				
				//補齊每頁格子數
				if( result.length % PageUnit != 0 ){
					for(let i = 0 ; i < PageUnit - result.length % PageUnit ; i++ ){
						table += "<tr style='height:37px;'><td></td><td></td></tr>";
					}
				}
				
				table += "</table></div>";
				
				$('#pagination_3').show();
				$('#pagination_3').html(pagination);
				
			} else {
				table = "<h1>未檢索到任何到相關結果<h1>";
				$('#pagination_3').hide();
			}
			$('#ICaseSearchTable').html(table);
		},
		error: err => {
			console.log("SearchData_3_err:", err);
		}
	});
}

//維護分頁 篩選結果 to 資料呈現
function SearchDataRead_3(Id) {
	$.ajax({
		type: 'POST',
		url: 'PatientCaseDataBase.do',
		data: {
			"sign": "3",
			"Id": Id
		},
		success: response => {
			var result = $.parseJSON(response);
			
			$('#IPatientCaseId').val(result[0].Id);
			
			$('#ITitle').val(result[0].Title);
			$('#IAuthor').val(result[0].Author);
			$('#IAges').val(result[0].Ages);
			$('#IPublish').val(result[0].Publish);
			
			$('#IGender').val(result[0].Gender);
			$('#IAge').val(result[0].Age);
			$('#IHeight').val(result[0].Height);
			$('#IWeight').val(result[0].Weight);
			$('#IMarriage').val(result[0].Marriage);
			$('#IPOResidence').val(result[0].Place_of_Residence);
			$('#ICareer').val(result[0].Career);
			
			$('#IChiefComplaint').val(result[0].Chief_Complaint);
			
			$('#ICurrentMedicalHistoryContent').val(result[0].Current_Medical_History);
			$('#IPastMedicalHistoryContent').val(result[0].Past_Medical_History);
			$('#IPersonalHistoryContent').val(result[0].Personal_History);
			$('#IFamilyHistoryContent').val(result[0].Family_History);

			$('#IObservationDiagnosisOrigin').val(result[0].Inspection_Origin);
			$('#IListenDiagnosisOrigin').val(result[0].Listen_and_Smelling_Examination_Origin);
			$('#IAskDiagnosisOrigin').val(result[0].Inquiry_Origin);
			$('#IPulseDiagnosisOrigin').val(result[0].Palpation_Origin);

			$('#IWesternMedicalDiagnosis').val(result[0].Western_Medical_Diagnosis);
			$('#IDiseaseDifferentiation').val(result[0].Disease_Differentiation);
			$('#ISyndromeDifferentiation').val(result[0].Syndrome_Differentiation);
			$('#ITreatmentPrinciples').val(result[0].Treatment_Principles);
			$('#IPrescription').val(result[0].Prescription);
		},
		error: err => {
			console.log("SearchDataRead_3_err:", err);
		}
	});
	
	$.ajax({
		type: 'POST',
		url: 'PatientCaseDataBase.do',
		data: {
			"sign": "4",
			"Id": Id
		},
		success: response => {
			var result = $.parseJSON(response);
			$('#IObservationDiagnosis').val(result[0].Cut_Inspection);
			$('#IListenDiagnosis').val(result[0].Cut_Listen_and_Smelling_Examination);
			$('#IAskDiagnosis').val(result[0].Cut_Inquiry);
			$('#IPulseDiagnosis').val(result[0].Cut_Palpation);
		},
		error: err => {
			console.log("SearchDataRead_Standard_3_err:", err);
		}
	});
}

//維護頁面 新增病例
function NewCase() {

	if($('#IHeight').val())$('#IHeight').val(parseFloat($('#IHeight').val()));
	if($('#IWeight').val())$('#IWeight').val(parseFloat($('#IWeight').val()));
	
	if (AlertContent = InsertCheck()){
		alert(AlertContent);
		return;
	}

	if ($('#IPatientCaseId').val() != "0") {
		if (!confirm("已存在之病例，確定要另存新病例?"))
			return;
	}

	$.ajax({
		type: 'POST',
		url: 'PatientCaseDataBase.do',
		data: {
			"sign": "6",
			"Title": $('#ITitle').val(),
			"Author": $('#IAuthor').val(),
			"Ages": $('#IAges').val(),
			"Publish": $('#IPublish').val(),

			"Gender": $('#IGender').val(),
			"Age": $('#IAge').val(),
			"Height": $('#IHeight').val(),
			"Weight": $('#IWeight').val(),
			"Marriage": $('#IMarriage').val(),
			"Place_of_Residence": $('#IPOResidence').val(),
			"Career": $('#ICareer').val(),

			"Chief_Complaint": $('#IChiefComplaint').val(),
			"Current_Medical_History": $('#ICurrentMedicalHistoryContent').val(),
			"Past_Medical_History": $('#IPastMedicalHistoryContent').val(),
			"Personal_History": $('#IPersonalHistoryContent').val(),
			"Family_History": $('#IFamilyHistoryContent').val(),

			"Inspection_Origin": $('#IObservationDiagnosisOrigin').val(),
			"Listen_and_Smelling_Examination_Origin": $('#IListenDiagnosisOrigin').val(),
			"Inquiry_Origin": $('#IAskDiagnosisOrigin').val(),
			"Palpation_Origin": $('#IPulseDiagnosisOrigin').val(),

			"Western_Medical_Diagnosis": $('#IWesternMedicalDiagnosis').val(),
			"Disease_Differentiation": $('#IDiseaseDifferentiation').val(),
			"Syndrome_Differentiation": $('#ISyndromeDifferentiation').val(),
			"Treatment_Principles": $('#ITreatmentPrinciples').val(),
			"Prescription": $('#IPrescription').val()
		},
		success: response => {
			$.ajax({
				type: 'POST',
				url: 'PatientCaseDataBase.do',
				data: {
					"sign": "7",
					"Gender": $('#IGender').val(),
					"Cut_Inspection": $('#IObservationDiagnosis').val(),
					"Cut_Listen_and_Smelling_Examination": $('#IListenDiagnosis').val(),
					"Cut_Inquiry": $('#IAskDiagnosis').val(),
					"Cut_Palpation": $('#IPulseDiagnosis').val(),
				},
				error: err => {
					console.log("NewCase_Standard_err:", err);
					return;
				}
			});
			window.scrollTo(0, 0);
			alert("病例新增成功");
			//清空維護分頁
			RefreshInput(true);
			//瀏覽器重新整理
			//location.reload(true);
			//更新各頁表單
			SearchDatas_1();
			SearchDatas_2();
			SearchDatas_3();
		},
		error: err => {
			console.log("NewCase_err:", err);
			return;
		}
	});
		
}

//維護頁面 清空頁面
function RefreshInput(ConfirmTag) {
	if (ConfirmTag) {
		
		$('#IPatientCaseId').val('0');
		
		//CSS清除錯誤提醒顯示
		$('#ITitle').removeClass("Inputalert");
		$('#IAuthor').removeClass("Inputalert");
		$('#IAges').removeClass("Inputalert");
		$('#IPublish').removeClass("Inputalert");
		$('#IAge').removeClass("Inputalert");
		$('#IGender').removeClass("Inputalert");
		$('#IHeight').removeClass("Inputalert");
		$('#IWeight').removeClass("Inputalert");
				
		$('#ITitle').val('');
		$('#IAuthor').val('');
		$('#IAges').val('');
		$('#IPublish').val('');

		$('#IGender').val('');
		$('#IAge').val('');
		$('#IHeight').val('');
		$('#IWeight').val('');
		$('#IMarriage').val('');
		$('#IPOResidence').val('');
		$('#ICareer').val('');

		$('#IChiefComplaint').val('');
		$('#ICurrentMedicalHistoryContent').val('');
		$('#IPastMedicalHistoryContent').val('');
		$('#IPersonalHistoryContent').val('');
		$('#IFamilyHistoryContent').val('');

		$('#IObservationDiagnosis').val('');
		$('#IListenDiagnosis').val('');
		$('#IAskDiagnosis').val('');
		$('#IPulseDiagnosis').val('');

		$('#IObservationDiagnosisOrigin').val('');
		$('#IListenDiagnosisOrigin').val('');
		$('#IAskDiagnosisOrigin').val('');
		$('#IPulseDiagnosisOrigin').val('');
		
		$('#IWesternMedicalDiagnosis').val('');
		$('#IDiseaseDifferentiation').val('');
		$('#ISyndromeDifferentiation').val('');
		$('#ITreatmentPrinciples').val('');
		$('#IPrescription').val('');

		
	}
}

//維護頁面 更新病例
function UpdateThisCase(ConfirmTag) {

	if($('#IHeight').val())$('#IHeight').val(parseFloat($('#IHeight').val()));
	if($('#IWeight').val())$('#IWeight').val(parseFloat($('#IWeight').val()));
	
	if (AlertContent = InsertCheck()){
		alert(AlertContent);
		return;
	}
	
	//修改資料庫用
	//自動在更新完換下一個病例
	//var ThisId = $('#IPatientCaseId').val();
	//NextId = parseInt(ThisId) + 1 ;
	//console.log(NextId);
	//完成後註解或刪除
	
	if (ConfirmTag) {

		if ($('#IPatientCaseId').val() == "0") {
			alert("無法修改新病例");
			return;
		}

		$.ajax({
			type: 'POST',
			url: 'PatientCaseDataBase.do',
			data: {
				"sign": "8",
				"Id": $('#IPatientCaseId').val(),

				"Title": $('#ITitle').val(),
				"Author": $('#IAuthor').val(),
				"Ages": $('#IAges').val(),
				"Publish": $('#IPublish').val(),

				"Gender": $('#IGender').val(),
				"Age": $('#IAge').val(),
				"Height": $('#IHeight').val(),
				"Weight": $('#IWeight').val(),
				"Marriage": $('#IMarriage').val(),
				"Place_of_Residence": $('#IPOResidence').val(),
				"Career": $('#ICareer').val(),

				"Chief_Complaint": $('#IChiefComplaint').val(),
				"Current_Medical_History": $('#ICurrentMedicalHistoryContent').val(),
				"Past_Medical_History": $('#IPastMedicalHistoryContent').val(),
				"Personal_History": $('#IPersonalHistoryContent').val(),
				"Family_History": $('#IFamilyHistoryContent').val(),

				"Inspection_Origin": $('#IObservationDiagnosisOrigin').val(),
				"Listen_and_Smelling_Examination_Origin": $('#IListenDiagnosisOrigin').val(),
				"Inquiry_Origin": $('#IAskDiagnosisOrigin').val(),
				"Palpation_Origin": $('#IPulseDiagnosisOrigin').val(),

				"Disease_Differentiation": $('#IDiseaseDifferentiation').val(),
				"Syndrome_Differentiation": $('#ISyndromeDifferentiation').val(),
				"Treatment_Principles": $('#ITreatmentPrinciples').val(),
				"Prescription": $('#IPrescription').val(),

				"Western_Medical_Diagnosis": $('#IWesternMedicalDiagnosis').val()
			},
			success: response => {
				$.ajax({
					type: 'POST',
					url: 'PatientCaseDataBase.do',
					data: {
						"sign": "9",
						"Id": $('#IPatientCaseId').val(),
						"Gender": $('#IGender').val(),
						"Cut_Inspection": $('#IObservationDiagnosis').val(),
						"Cut_Listen_and_Smelling_Examination": $('#IListenDiagnosis').val(),
						"Cut_Inquiry": $('#IAskDiagnosis').val(),
						"Cut_Palpation": $('#IPulseDiagnosis').val()
					},
					error: err => {
						console.log("UpdateThisCase_Standard_err:", err);
						return;
					}
				});
				
				window.scrollTo(0, 0);
				alert("病例更新成功");
				//清空維護分頁
				RefreshInput(true);
				//瀏覽器重新整理
				//location.reload(true);
				//更新各頁表單
				SearchDatas_1();
				SearchDatas_2();
				SearchDatas_3();
				
				//修改資料庫用
				//SearchDataRead_3(NextId);
				//完成後註解或刪除
			},
			error: err => {
				console.log("UpdateThisCase_err:", err);
				return;
			}
		});
	}
}

//維護頁面 刪除病例
function DeleteThisCase(ConfirmTag) {
	if (ConfirmTag) {

		if ($('#IPatientCaseId').val() == "0") {
			alert("無法刪除不存在於資料庫之病例");
			return;
		}

		$.ajax({
			type: 'POST',
			url: 'PatientCaseDataBase.do',
			data: {
				"sign": "12",
				"Id": $('#IPatientCaseId').val()
			},
			success: response => {
				window.scrollTo(0, 0);
				alert("病例刪除成功");
				//清空維護分頁
				RefreshInput(true);
				//瀏覽器重新整理
				//location.reload(true);
				//更新各頁表單
				SearchDatas_1();
				SearchDatas_2();
				SearchDatas_3();
			},
			error: err => {
				console.log("DeleteThisCase_err:", err);
			}
		});
	}
}

//單筆標準化 檢索系統介面
function SingleCaseStandard(CaseID){

	$.ajax({
		type: 'POST',
		url: 'PatientCaseDataBase.do',
		data: {
			"sign": "4",
			"Id": CaseID
		},
		success: response => {
			var result = $.parseJSON(response);
			var Mixed_Cut_Diagnosis = result[0].Cut_Inspection + result[0].Cut_Listen_and_Smelling_Examination + result[0].Cut_Inquiry + result[0].Cut_Palpation;
			findstandard(CaseID, Mixed_Cut_Diagnosis);
		},
		error: err => {
			console.log("SearchDataRead_Standard_3_err:", err);
		}
	});

	
}


//症狀標準化 copy from 引導式問診
function findstandard(CaseID, Original_Symptoms) {

	$.ajax({
		url: "SymptomsStandardbyMySQL",
		type: "POST",
		data: {
			"Original_Symptoms": Original_Symptoms　
		},
		dataType: "json",
		success: response => {
			var text_indb = "";
			var text_notindb = "";
			response.map( item => {
				for (i = 0; i < item.modified_Data_list.length; i++) {
					if (item.modified_Data_list[i].standard_symptoms != "不存在資料庫") {
						text_indb += item.modified_Data_list[i].standard_symptoms + "。";
					} else {
						if (item.original_symptoms == "") continue;
						text_notindb += item.original_symptoms + "。";
					}
				}
			});
			
			$.ajax({
				type: 'POST',
				url: 'PatientCaseDataBase.do',
				data: {
					"sign": "10",
					"Id": CaseID ,
					"Symptoms_Standard": text_indb ,
					"Symptoms_Unstandard": text_notindb
				},error: err => {
					console.log("findstandard_update_database_err:", err);
					return;
				}
			});

		},
		error: err => {
			console.log("findstandard_ajax_err:", err);
		}
	});
	

}

function CSVExportBtn(){
	
	let data = document.getElementById("CSV_temp").innerHTML;
	
	let LinktoCSV = document.createElement('a');
	 	LinktoCSV.download = "Export_"+NowTimeString()+".csv";
	 	
		if ( window.URL != null ) {
	        let code = encodeURIComponent( data );
	        if ( navigator.appVersion.indexOf("Win")==-1 ) {
	        	LinktoCSV.href = "data:application/csv;charset=utf-8," + code;
	        } else {
	        	LinktoCSV.href = "data:application/csv;charset=utf-8,%EF%BB%BF" + code;
	        }
	    }
		
	LinktoCSV.click();
}