﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using org.apache.pdfbox.pdmodel;
using org.apache.pdfbox.util;

namespace Parse
{
    public partial class ParsePdf : System.Web.UI.Page
    {
        public static string res;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnParse_Click(object sender, EventArgs e)
        {
            //儲存位置
            String savePath = @"C:\upload";
            if (FileUpload1.HasFile)
            {
                String fileName = FileUpload1.FileName;
                savePath += fileName;
                FileUpload1.SaveAs(savePath);
                //Label1.Text = "執行結果：成功";
            }
            else
            {
                //Label1.Text = "執行結果：失敗";
            }
            PDDocument doc = PDDocument.load(savePath);
            //Stripper是PDF轉換過的文字
            PDFTextStripper Stripper = new PDFTextStripper();
            //TextBox1.Text = (Stripper.getText(doc));
            //https://stackoverflow.com/questions/10709821/find-text-in-string-with-c-sharp
            res = Stripper.getText(doc);

            getTitile();
            getAuthor();
            getGender();
            getAge();
            Height();
            Weight();
            Marriage();
            Place();
            Career();
            ChiefComplaint();
            CurrentMedicalHistoryContent();
            PastMedicalHistoryContent();
            PersonalHistoryContent();
            FamilyHistoryContent();
            ObservationDiagnosisOrigin();
            ListenDiagnosisOrigin();
            AskDiagnosis();
            PulseDiagnosis();
            WesternMedicalDiagnosis();
            DiseaseDifferentiation();
            SyndromeDifferentiation();
            TreatmentPrinciples();
            Prescription();
        }
        //中間字詞範圍
        private static string getBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                //內文搜尋第一個符合的字串當索引第一項
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                //內文搜尋符合的字串當索引最後一項
                End = strSource.IndexOf(strEnd, Start);
                //回傳內文擷取文字該位置
                return strSource.Substring(Start, End-Start);
            }
            else
            {
                return "";
            }
        }
        //篇名
        private void getTitile()
        {
            string cutword = "head" + getBetween(res, "", "病例報告") + "病例報告";
            string title = getBetween(cutword,"head","病例報告") + "病例報告";

            ITitle.Text = cutword;
        }
        //作者
        private void getAuthor()
        {

        }
        //性別
        private void getGender()
        {
            //if (res.Contains("男"))
            //{
            //    IGender.Text = "男性";
            //}
            //else if (res.Contains("女"))
            //{
            //    IGender.Text = "女性";
            //}
            //string s1 = getBetween(res, "性別", "姓");
            //IGender.Text = s1;
            //int index;
            //string age = "性別：";
            //if (res.Contains(age))
            //{
            //    index = res.IndexOf(age, 0) - 1;
            //    string s1 = "head" + res.Substring(index);
            //    //string s2 = getBetween(s1, "head", "歲");
            //    IGender.Text = s1;
            //}
            string s1 = getBetween(res, "性別：", "性");
            IGender.Text = s1;

        }
        //年齡
        private void getAge()
        {
            int index;
            string age = "歲";
            if (res.Contains(age))
            {
                index = res.IndexOf(age,0) - 3;
                string s1 = "head"+res.Substring(index);
                string s2 = getBetween(s1,"head","歲");
                IAge.Text = s2;
            }
        }
        //身高
        private void Height()
        {
            string h = getBetween(res, "身高：", "cm");
            IHeight.Text = h;
        }
        //體重
        private void Weight()
        {
            string w = getBetween(res, "體重：", "kg");
            IWeight.Text = w;
        }
        //婚姻
        private void Marriage()
        {
            string m = getBetween(res, "婚姻：", "婚")+"婚";
            IMarriage.Text = m;
        }
        //居住地
        private void Place()
        {
            IPOResidence.Text  = "沒有資料";
        }
        //職業
        private void Career()
        {
            ICareer.Text = "沒有資料";
        }
        //主述
        private void ChiefComplaint()
        {
            string m = getBetween(res, "主訴", "現病史");
            IChiefComplaint.Text = m;
        }
        //現病史
        private void CurrentMedicalHistoryContent()
        {
            string m = getBetween(res, "現病史", "聯絡人");
            TCurrentMedicalHistory.Text = m;
        }
        //過去病史
        private void PastMedicalHistoryContent()
        {
            string m = getBetween(res, "過去病史", "個人史");
            TPastMedicalHistory.Text = m;
        }
        //個人史
        private void PersonalHistoryContent()
        {
            
        }
        //家族病史
        private void FamilyHistoryContent()
        {

        }
        //望
        private void ObservationDiagnosisOrigin()
        {

        }
        //聞
        private void ListenDiagnosisOrigin()
        {

        }
        //問
        private void AskDiagnosisOrigin()
        {

        }
        //切
        private void PulseDiagnosisOrigin()
        {

        }
        //症狀字詞
        //望
        private void ObservationDiagnosis()
        {

        }
        //聞
        private void ListenDiagnosis()
        {

        }
        //問
        private void AskDiagnosis()
        {

        }
        //切
        private void PulseDiagnosis()
        {

        }
        //西醫診斷
        private void WesternMedicalDiagnosis()
        {

        }
        //辨病
        private void DiseaseDifferentiation()
        {

        }
        //辨證
        private void SyndromeDifferentiation()
        {

        }
        //治則
        private void TreatmentPrinciples()
        {

        }
        //方藥
        private void Prescription()
        {
            
        }
    }
}