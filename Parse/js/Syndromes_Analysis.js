function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

function Single_Syndromes_Analysis(CaseID){
	let temp = $('#SyndromesAnalysisTag').html();
	$('#SyndromesAnalysisTag').html("<h3>辨證使用之標準症狀 ： <small><span class='glyphicon glyphicon-refresh w3-spin'></span></small></h3>");
	
	$.ajax({
		type: 'POST',
		url: 'PatientCaseDataBase.do',
		data: {
			"sign": "4",
			"Id": CaseID
		},
		success: response => {
			var result = $.parseJSON(response);
			let StandardList = result[0].Symptoms_Standard.slice(0,-1).split('\u3002');
			let Gender =  result[0].Gender=='男'?" 1":" 2";
			var StandardData = JSON.stringify(StandardList);
			$.ajax({
				type : "POST",
				url : "InteractiveInquiry.do",
				data :{
					input : StandardData,
					patientnumber: Gender
				},
				dataType : 'json',
				success : DataList => {
					
					var dict = {};
					
					//-------------虛證分數 20筆------------- 
					dict["心氣虛"] = DataList.Heart.HeartChi[4];
					dict["心血虛"] = DataList.Heart.HeartBlood[4];
						dict["心陰虛"] = DataList.Heart.HeartYin[4];
						dict["心陽虛"] = DataList.Heart.HeartYun[4];
						dict["肝血虛"] = DataList.Liver.LiverBlood[4];
						dict["肝陰虛"] = DataList.Liver.LiverYin[4];
						dict["脾氣虛"] = DataList.Spleen.SpleenChi[4];
						dict["脾陽虛"] = DataList.Spleen.SpleenYun[4];
						dict["脾氣陷"] = DataList.Spleen.SpleenChiTrap[4];
						dict["脾不統血"] = DataList.Spleen.SpleenBlood[4];
						dict["胃陰虛"] = DataList.Spleen.SpleenYin[4];
						dict["肺氣虛"] = DataList.Lung.LungChi[4];
						dict["肺陰虛"] = DataList.Lung.LungYin[4];
						dict["腎精不足"] = DataList.Kidney.KidneyChi[4];
						dict["腎不納氣"] = DataList.Kidney.KidneyAdmit[4];
						dict["腎氣不固"] = DataList.Kidney.KidneySolid[4];
						dict["腎陰虛"] = DataList.Kidney.KidneyYin[4];
						dict["腎陽虛"] = DataList.Kidney.KidneyYun[4];
						dict["心陽暴脫"] = DataList.Heart.HeartYunBau[5];
						dict["大腸陽虛"] = DataList.Lung.Large_intestineYun[4];
						
					//-------------實證分數 47筆-------------
					
					//衛表
					dict["風襲衛表"] = DataList.Excess.defensive.wind[0].total;
					dict["寒襲衛表"] = DataList.Excess.defensive.cold[0].total;
					dict["暑襲衛表"] = DataList.Excess.defensive.summer[0].total;
					dict["濕襲衛表"] = DataList.Excess.defensive.wet[0].total;
					dict["燥襲衛表"] = DataList.Excess.defensive.dry[0].total;
					dict["熱襲衛表"] = DataList.Excess.defensive.heat[0].total;
					
					//肌膚
					dict["風襲肌膚"] = DataList.Excess.skin.wind[0].total;
					dict["濕襲肌膚"] = DataList.Excess.skin.wet[0].total;
					
					//經絡
					dict["風襲經絡"] = DataList.Excess.meridian.wind[0].total;
					dict["寒襲經絡"] = DataList.Excess.meridian.cold[0].total;
					
					//關節
					dict["風襲關節"] = DataList.Excess.articulation.wind[0].total;
					dict["濕襲關節"] = DataList.Excess.articulation.wet[0].total;
					
					//心
					dict["心脈痺阻(寒凝)"] = DataList.Excess.heart.cold[0].total;
					dict["心火亢盛"] = DataList.Excess.heart.fiery[0].total;
					dict["心脈痺阻(痰聚)"] = DataList.Excess.heart.sputum[0].total;
					dict["痰火擾心(鬱)"] = DataList.Excess.heart.sputum[1].total;
					dict["痰火擾心(躁)"] = DataList.Excess.heart.sputum[2].total;
					dict["痰火擾心(外感熱)"] = DataList.Excess.heart.sputum[3].total;
					dict["痰迷心竅"] = DataList.Excess.heart.sputum[4].total;
					dict["心脈痺阻(血瘀)"] = DataList.Excess.heart.blood_stasis[0].total;
					dict["心脈痺阻(氣滯)"] = DataList.Excess.heart.qi_stagnation[0].total;
					
					//小腸
					dict["小腸實熱"] = DataList.Excess.small_intestine.heat[0].total;
					
					//肺
					dict["寒邪客肺"] = DataList.Excess.lung.cold[0].total;
					dict["熱邪壅肺"] = DataList.Excess.lung.heat[0].total;
					dict["痰濕阻肺"] = DataList.Excess.lung.sputum[0].total;
					dict["痰熱壅肺"] = DataList.Excess.lung.sputum[1].total;
					
					//大腸
					dict["大腸濕熱"] = DataList.Excess.large_intestine.wet[0].total;
					dict["大腸液虧"] = DataList.Excess.large_intestine.dry[0].total;
					dict["大腸實熱"] = DataList.Excess.large_intestine.heat[0].total;
					
					//脾
					dict["寒邪客脾"] = DataList.Excess.spleen.cold[0].total;
					dict["濕邪客脾"] = DataList.Excess.spleen.wet[0].total;
					dict["熱邪客脾"] = DataList.Excess.spleen.heat[0].total;
					
					//胃
					dict["寒邪客胃"] = DataList.Excess.stomach.cold[0].total;
					dict["熱邪客胃"] = DataList.Excess.stomach.heat[0].total;
					dict["胃氣上逆"] = DataList.Excess.stomach.qi_stagnation[0].total;
					dict["食滯胃脘"] = DataList.Excess.stomach.diet[0].total;
					
					//肝
					dict["肝陽化風"] = DataList.Excess.liver.wind[0].total;
					dict["陰虛動風"] = DataList.Excess.liver.wind[1].total;
					dict["血虛生風"] = DataList.Excess.liver.wind[2].total;
					dict["熱極生風"] = DataList.Excess.liver.wind[3].total;
					dict["寒滯肝脈"] = DataList.Excess.liver.cold[0].total;
					dict["肝膽濕熱"] = DataList.Excess.liver.wet[0].total;
					dict["肝火上炎"] = DataList.Excess.liver.fiery[0].total;
					dict["肝陽上亢"] = DataList.Excess.liver.fiery[1].total;
					dict["肝氣鬱結"] = DataList.Excess.liver.qi_stagnation[0].total;
					
					//膽
					dict["膽郁痰擾"] = DataList.Excess.gallbladder.sputum[0].total;
					
					//腎
					
					//膀胱
					dict["膀胱濕熱"] = DataList.Excess.urinary_bladder.wet[0].total;
						
					//分數排序
					// Create items array
						var items = Object.keys(dict).map(function(key) {return [key, dict[key]];});

						// Sort the array based on the second element
						items.sort(function(first, second) {return second[1] - first[1];});
						
						var IfNext = true ;
						var SliceCount = 0;
						for(let i = 0; IfNext ; i++){
							if(items[i][1] > 0.5){
								SliceCount++;
							}else{
								if(SliceCount < 5){
									SliceCount++;
								}else{
									IfNext = false;
								}
							}
							if(SliceCount == items.length) IfNext = false;
						}
						DifferentationResult = items.slice(0,SliceCount);
						
						var DifferentationOutput = "";
						for(let i = 0; i < DifferentationResult.length ; i++){
							DifferentationOutput += "("+(i+1)+")"+DifferentationResult[i][0]+"("+DifferentationResult[i][1]+")。\n";
						}
						
						$.ajax({
							type: 'POST',
							url: 'PatientCaseDataBase.do',
							data: {
								"sign": "11",
								"Id": CaseID ,
								"Syndromes_Analysis_Result": DifferentationOutput
							},success: response => {
								$("#DifferentationResult").html(DifferentationOutput);
								$('#SyndromesAnalysisTag').html(temp);
							},error: err => {
								console.log("findstandard_update_database_err:", err);
								return;
							}
						});
						
				},
				error : err => {
					console.log("Single_Syndromes_Analysis_err:", err);
				}
			});
		},
		error: err => {
			console.log("Single_Syndromes_Analysis_err:", err);
		}
	});
}

function DataBase_Syndromes_Analysis_Refresh(ConfirmTag){
	if(ConfirmTag){
		$.ajax({
			type: 'POST',
			url: 'PatientCaseDataBase.do',
			data: {
				"sign": "2"
			},
			success: response => {
				var result2 = $.parseJSON(response);
				var ResultCount = 0;
				for (let i = 0; i < result2.length ; i++){
					
					if(result2[i].Symptoms_Standard == "") {
						ResultCount++;
						continue ;
					}
						sleep((i-ResultCount)*10000).then(() => {
							
						let Id = result2[i].Id;
						let Gender = result2[i].Gender=='男'?" 1":" 2";
						let StandardList = result2[i].Symptoms_Standard.slice(0,-1).split('\u3002');
						
						let StandardData = JSON.stringify(StandardList);
						
						$.ajax({
							type : "POST",
							url : "InteractiveInquiry.do",
							data :{
								CaseID : Id,
								input : StandardData,
								patientnumber: Gender
							},
							dataType : 'json',
							success : DataList => {
								
								var dict = {};
								
								//-------------虛證分數 20筆------------- 
								dict["心氣虛"] = DataList.Heart.HeartChi[4];
								dict["心血虛"] = DataList.Heart.HeartBlood[4];
				  				dict["心陰虛"] = DataList.Heart.HeartYin[4];
				  				dict["心陽虛"] = DataList.Heart.HeartYun[4];
				  				dict["肝血虛"] = DataList.Liver.LiverBlood[4];
				  				dict["肝陰虛"] = DataList.Liver.LiverYin[4];
				  				dict["脾氣虛"] = DataList.Spleen.SpleenChi[4];
				  				dict["脾陽虛"] = DataList.Spleen.SpleenYun[4];
				  				dict["脾氣陷"] = DataList.Spleen.SpleenChiTrap[4];
				  				dict["脾不統血"] = DataList.Spleen.SpleenBlood[4];
				  				dict["胃陰虛"] = DataList.Spleen.SpleenYin[4];
				  				dict["肺氣虛"] = DataList.Lung.LungChi[4];
				  				dict["肺陰虛"] = DataList.Lung.LungYin[4];
				  				dict["腎精不足"] = DataList.Kidney.KidneyChi[4];
				  				dict["腎不納氣"] = DataList.Kidney.KidneyAdmit[4];
				  				dict["腎氣不固"] = DataList.Kidney.KidneySolid[4];
				  				dict["腎陰虛"] = DataList.Kidney.KidneyYin[4];
				  				dict["腎陽虛"] = DataList.Kidney.KidneyYun[4];
				  				dict["心陽暴脫"] = DataList.Heart.HeartYunBau[5];
				  				dict["大腸陽虛"] = DataList.Lung.Large_intestineYun[4];
				  				
								//-------------實證分數 47筆-------------
								
								//衛表
								dict["風襲衛表"] = DataList.Excess.defensive.wind[0].total;
								dict["寒襲衛表"] = DataList.Excess.defensive.cold[0].total;
								dict["暑襲衛表"] = DataList.Excess.defensive.summer[0].total;
								dict["濕襲衛表"] = DataList.Excess.defensive.wet[0].total;
								dict["燥襲衛表"] = DataList.Excess.defensive.dry[0].total;
								dict["熱襲衛表"] = DataList.Excess.defensive.heat[0].total;
								
								//肌膚
								dict["風襲肌膚"] = DataList.Excess.skin.wind[0].total;
								dict["濕襲肌膚"] = DataList.Excess.skin.wet[0].total;
								
								//經絡
								dict["風襲經絡"] = DataList.Excess.meridian.wind[0].total;
								dict["寒襲經絡"] = DataList.Excess.meridian.cold[0].total;
								
								//關節
								dict["風襲關節"] = DataList.Excess.articulation.wind[0].total;
								dict["濕襲關節"] = DataList.Excess.articulation.wet[0].total;
								
								//心
								dict["心脈痺阻(寒凝)"] = DataList.Excess.heart.cold[0].total;
								dict["心火亢盛"] = DataList.Excess.heart.fiery[0].total;
								dict["心脈痺阻(痰聚)"] = DataList.Excess.heart.sputum[0].total;
								dict["痰火擾心(鬱)"] = DataList.Excess.heart.sputum[1].total;
								dict["痰火擾心(躁)"] = DataList.Excess.heart.sputum[2].total;
								dict["痰火擾心(外感熱)"] = DataList.Excess.heart.sputum[3].total;
								dict["痰迷心竅"] = DataList.Excess.heart.sputum[4].total;
								dict["心脈痺阻(血瘀)"] = DataList.Excess.heart.blood_stasis[0].total;
								dict["心脈痺阻(氣滯)"] = DataList.Excess.heart.qi_stagnation[0].total;
								
								//小腸
								dict["小腸實熱"] = DataList.Excess.small_intestine.heat[0].total;
								
								//肺
								dict["寒邪客肺"] = DataList.Excess.lung.cold[0].total;
								dict["熱邪壅肺"] = DataList.Excess.lung.heat[0].total;
								dict["痰濕阻肺"] = DataList.Excess.lung.sputum[0].total;
								dict["痰熱壅肺"] = DataList.Excess.lung.sputum[1].total;
								
								//大腸
								dict["大腸濕熱"] = DataList.Excess.large_intestine.wet[0].total;
								dict["大腸液虧"] = DataList.Excess.large_intestine.dry[0].total;
								dict["大腸實熱"] = DataList.Excess.large_intestine.heat[0].total;
								
								//脾
								dict["寒邪客脾"] = DataList.Excess.spleen.cold[0].total;
								dict["濕邪客脾"] = DataList.Excess.spleen.wet[0].total;
								dict["熱邪客脾"] = DataList.Excess.spleen.heat[0].total;
								
								//胃
								dict["寒邪客胃"] = DataList.Excess.stomach.cold[0].total;
								dict["熱邪客胃"] = DataList.Excess.stomach.heat[0].total;
								dict["胃氣上逆"] = DataList.Excess.stomach.qi_stagnation[0].total;
								dict["食滯胃脘"] = DataList.Excess.stomach.diet[0].total;
								
								//肝
								dict["肝陽化風"] = DataList.Excess.liver.wind[0].total;
								dict["陰虛動風"] = DataList.Excess.liver.wind[1].total;
								dict["血虛生風"] = DataList.Excess.liver.wind[2].total;
								dict["熱極生風"] = DataList.Excess.liver.wind[3].total;
								dict["寒滯肝脈"] = DataList.Excess.liver.cold[0].total;
								dict["肝膽濕熱"] = DataList.Excess.liver.wet[0].total;
								dict["肝火上炎"] = DataList.Excess.liver.fiery[0].total;
								dict["肝陽上亢"] = DataList.Excess.liver.fiery[1].total;
								dict["肝氣鬱結"] = DataList.Excess.liver.qi_stagnation[0].total;
								
								//膽
								dict["膽郁痰擾"] = DataList.Excess.gallbladder.sputum[0].total;
								
								//腎
								
								//膀胱
								dict["膀胱濕熱"] = DataList.Excess.urinary_bladder.wet[0].total;
				  				
								//分數排序
								// Create items array
				  				var items = Object.keys(dict).map(function(key) {return [key, dict[key]];});
	
				  				// Sort the array based on the second element
				  				items.sort(function(first, second) {return second[1] - first[1];});
				  				
				  				var IfNext = true ;
				  				var SliceCount = 0;
				  				for(let i = 0; IfNext ; i++){
				  					if(items[i][1] > 0.5){
				  						SliceCount++;
				  					}else{
				  						if(SliceCount < 5){
				  							SliceCount++;
				  						}else{
				  							IfNext = false;
				  						}
				  					}
				  					if(SliceCount == items.length) IfNext = false;
				  				}
				  				DifferentationResult = items.slice(0,SliceCount);
				  				
				  				var DifferentationOutput = "";
				  				for(let i = 0; i < DifferentationResult.length ; i++){
				  					DifferentationOutput += "("+(i+1)+")"+DifferentationResult[i][0]+"("+DifferentationResult[i][1]+")。\n";
				  				}
				  				
				  				$.ajax({
				  					type : "POST",
				  					url : "PatientCaseDataBase.do",
				  					data :{
				  						sign : "11",
				  						Id : DataList.CaseID,
				  						Syndromes_Analysis_Result : DifferentationOutput
				  					},success : () => {
				  						console.log(DataList.CaseID);
				  						console.log(DifferentationOutput);
				  					},error : err => {
										console.log("DataBase_Syndromes_Update_err:", err);
									}
								});
				  				
							},
							error : function(err){
								console.log("Syndromes_Analysis_err:", err);
							}
						});
					});
				}
			},error: err => {
				console.log("DataBase_Syndromes_Analysis_Refresh_err:", err);
			}
		});
	}
}

//from interactiveinquiry.jsp at 2018/07/09